{
    'name': "Books Inventory System",
    'description':"Manage your books shop their sale and purchase.. ",
    'author':"Aneeq Ul Haq",
    'depends':['base','mail'],
    'application':True,
    'data':[
        'views/main_view.xml', 
        'views/books_view.xml',
        'views/book_category_view.xml',
        'views/book_series_view.xml',
        'views/stakeholder_view.xml',
        'views/book_purchase_view.xml',
        'views/book_inventory_view.xml',
        'views/book_sale.xml',
        'reports/purchase_report.xml',

        # 'security/ir.model.access.csv',
        # 'security/security.xml',
           
        ]
}