from flectra import models, fields, api, _

class BooksTasks(models.Model):
    _name = "books.task" # Model
    _order = "name" 
    name = fields.Char('Name', required = True)
    category_id = fields.Many2one('book.category')
    edition = fields.Char('Edition')
    series_id = fields.Many2one('book.series')
    publisher_id = fields.Many2one('res.partner')
    author_id = fields.Many2one('res.partner') 
    date = fields.Date('Date')
    purchase_price = fields.Integer('Purchase Price')
    sale_price = fields.Integer('Sale price')

   


    #sale_id = fields.Many2one("book.sale") user for sale
    #purchase_id = fields.Many2one('book.purchase') # use as one to many in book purchase
    # active = fields.Boolean('Active', default = False )
    # priority = fields.Selection(
    #     [('0','low'),('1','normal'),('2','high')]
    # )
    # kanban_state = fields.Selection(
    #     [('normal', 'In Progress'),
    #     ('blocked', 'Blocked'), 
    #     ('done', 'Ready for next stage')],
    #     'Kanban State', default='normal')
    @api.constrains('name') # condition if book name already inserted
    def _check_name(self):
        for record in self:
            name_exist = self.search([('name','=',record.name)])
            if len(name_exist) > 1:
                raise ValueError("bookname already exists.....")

class BookCategory(models.Model):    
    _name = "book.category" # Model
    name = fields.Char('Category', required = True ) 
    active = fields.Boolean('Active?', default=True ) 
class BookSeries(models.Model):
    _name = "book.series" # Model
    name = fields.Char('Series', required = True ) 
    active = fields.Boolean('Active?', default=True )    
    