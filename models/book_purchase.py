from flectra import models, fields, api, _
import logging
class BookPurchase(models.Model):
    _name = "book.purchase"
    supplier = fields.Many2one('res.partner', domain="[('supplier','=',True)]" )
    books = fields.One2many(comodel_name='book.purchase.line', inverse_name= 'purchase_id') 
    date = fields.Date(string='Date')
    total_amount = fields.Integer('Total Aamount')
    discount_percentage = fields.Integer('Discount Percentage')
    after_discount_grand = fields.Integer('After Discount Grand')
    paid_amount = fields.Integer('Paid Amount')
    due_amount = fields.Integer('Due Amount')
    previous_amount = fields.Integer('Previous',compute="calculate_previous")


    @api.model
    def create(self, values):
        res = super(BookPurchase, self).create(values)
        for book_line in res.books:
            
            current_book = res.env['inventory.books'].search([('book_id','=', book_line.book_id.id) ] , limit= 1 )
            if not current_book: 
                current_book = self.env['inventory.books'].create({
                        "book_id": book_line.book_id.id,
                        "quantity": 0
                    })
            print("===========================",current_book.book_id.name)
            # logging.info(current_book.quantity)
            current_book.quantity += book_line.quantity
            #current_book.purchased_qty += book_line.quantity
        return res



 
    @api.onchange('books')
    # Calculate Total Amount 
    def calculateTotal(self):
        x = 0
        for book in self.books:
            x +=  book.sale_price
        self.total_amount = x
    
    @api.onchange('discount_percentage')
    #Calculate Discount 
    def calculateDis(self):
        total_net_amount =  self.total_amount - (self.total_amount * (self.discount_percentage  / 100))
        self.after_discount_grand = total_net_amount
        self.paid_amount = total_net_amount

    @api.onchange('paid_amount')
    #Calculate Due Amount
    def calculateDue(self):
        paidd = self.after_discount_grand - self.paid_amount
        self.due_amount = paidd

    @api.onchange('supplier')
    # Supplier Previous Due Amount
    def calculate_previous(self):
        supplier_due = self.env['book.purchase'].search([( 'supplier', '=', self.supplier.id )], limit = 1 )
        self.previous_amount = supplier_due.due_amount
        

            
class BookPurchaseLines(models.Model):
    _name="book.purchase.line"
    book_id = fields.Many2one('books.task', required = True ) 
    quantity = fields.Integer('Quantity')
    purchase_price = fields.Integer('Purchase_price')
    sale_price = fields.Integer('sale_price')
    purchase_id = fields.Many2one('book.purchase') # one to many relation in book.purchase

    @api.onchange("quantity")
    def calculatePrice(self): # quantity * prices
        purchase_price_new = 0
        sale_price_new = 0
        purchase_price_new = self.quantity * self.purchase_price
        sale_price_new = self.quantity * self.sale_price
        self.purchase_price = purchase_price_new
        self.sale_price = sale_price_new

    @api.onchange("book_id")
    def findPrices(self): # book prices
        bookss = self.env['books.task'].search([( 'id', '=',self.book_id.id )], limit = 1)
        #raise Exception(bookss)
        self.purchase_price = bookss.purchase_price
        self.sale_price = bookss.sale_price

    

   

class Inverntorybooks(models.Model):
    _name = "inventory.books"
    purchase_id = fields.Many2one('book.purchase')
    book_id = fields.Many2one('books.task')
    quantity = fields.Integer('Quantity')
    purchased_qty = fields.Integer("Purchased Quantity")
    saled_qty = fields.Integer('Saled Quantity')