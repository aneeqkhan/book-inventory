from flectra import models, fields, api, _
import logging
class BookSale(models.Model):
    _name = "book.sale"
    customer = fields.Many2one('res.partner', domain="[('customer','=',True)]" )
    books = fields.One2many(comodel_name="book.sale.line",inverse_name="book_sale_id")
    date = fields.Date('Date')
    total_amount = fields.Integer('Total Aamount')
    discount_percentage = fields.Integer('Discount Percentage')
    after_discount_grand = fields.Integer('After Discount Grand')
    paid_amount = fields.Integer('Paid Amount')
    due_amount = fields.Integer('Due Amount')


    @api.model
    def create(self, values):
        res = super(BookSale,self).create(values)
        for book_line in res.books:
            current_book = res.env['inventory.books'].search([('book_id','=', book_line.book_id.id) ] , limit= 1 )
            if not current_book:
                current_book = self.env['inventory.books'].create({
                    "book_id":book_line.book_id.id,
                    "quantity":0
                })
            current_book.quantity -= book_line.quantity

            current_book.saled_qty -= book_line.quantity

        return res
    @api.onchange('books') 
    def calculateTotal(self):
        x = 0
        for book in self.books:
            #total_qty_amount = book.quantity * book.sale_price
            x +=  book.sale_price
        self.total_amount = x

    @api.onchange('discount_percentage')
    def calculateDis(self):
        total_net_amount =  self.total_amount - (self.total_amount * (self.discount_percentage  / 100))
        self.after_discount_grand = total_net_amount
        self.paid_amount = total_net_amount
    
    @api.onchange('paid_amount')
    def calculateDue(self):
        paidd = self.after_discount_grand - self.paid_amount
        self.due_amount = paidd


class BookSaleLines(models.Model):
    _name = "book.sale.line"
    book_sale_id = fields.Many2one('book.sale' )
    book_id = fields.Many2one('books.task', required=True)
    
    quantity = fields.Integer('Quantity')
    purchase_price = fields.Integer('purchase Price')
    sale_price = fields.Integer('sale Price')

    @api.onchange('book_id')
    def find_qty(self):
        qty_inventory = self.env['inventory.books'].search([('book_id','=',self.book_id.id)])
        self.quantity = qty_inventory.quantity

       
    @api.onchange("quantity")
    def calculatePrice(self):
        purchase_price_new = 0
        sale_price_new = 0

        purchase_price_new = self.quantity * self.purchase_price
        sale_price_new = self.quantity * self.sale_price
        self.purchase_price = purchase_price_new
        self.sale_price = sale_price_new

    @api.onchange("book_id")
    def findPrices(self): 
        bookss = self.env['books.task'].search([( 'id', '=',self.book_id.id )], limit = 1)
        #raise Exception(bookss)
        self.purchase_price = bookss.purchase_price
        self.sale_price = bookss.sale_price
  
    def find_qty_purchase(self):
        purchase_qty = self.env['book.purchase.line'].search([('book_id', '=', self.book_id.id  )  ], limit = 1 )
        # self.quantity = purchase_qty.quantity

        # logging.info(purchase_qty.quantity)
        # print(purchase_qty.quantity)